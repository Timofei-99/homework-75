const express = require('express');
const router = express.Router();
const Vigenere = require('caesar-salad').Vigenere;


router.get('/', (req, res) => {
    res.send('GET');
});

router.post('/encode', (req, res) => {
    res.send( {
       encoded: Vigenere.Cipher(req.body.password).crypt(req.body.message),
    });
});

router.post('/decode', (req, res) => {
    res.send({
        decoded: Vigenere.Decipher(req.body.password).crypt(req.body.message),
    });
});


module.exports = router;