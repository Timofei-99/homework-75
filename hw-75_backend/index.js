const express = require('express');
const vigenere = require('./Vigenere/Vigenere');
const cors = require('cors');



const app = express();
app.use(express.json());
app.use(cors());

const port = 8000;
app.use('/code', vigenere);

app.listen(port, () => {
    console.log('We are on ' + port);
});