import axios from 'axios';

const axiosAPI = axios.create({
    baseUrl: 'http://localhost:8000/code',
})

export default axiosAPI;