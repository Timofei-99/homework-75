import Forms from "./components/Forms/Forms";
import './App.css';

const App = () => (
    <div className='Container'>
        <Forms/>
    </div>
);

export default App;
