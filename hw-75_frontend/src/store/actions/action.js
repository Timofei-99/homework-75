import axios from "axios";

export const POST_ENCODE_REQUEST = 'POST_ENCODE_REQUEST';
export const POST_ENCODE_SUCCESS = 'POST_ENCODE_SUCCESS';
export const POST_ENCODE_FAILURE = 'POST_ENCODE_FAILURE';

export const POST_DECODE_REQUEST = 'POST_DECODE_REQUEST';
export const POST_DECODE_SUCCESS = 'POST_DECODE_SUCCESS';
export const POST_DECODE_FAILURE = 'POST_DECODE_FAILURE';

export const CHANGE_INPUTS = 'CHANGE_INPUTS';

export const changeInputs = (e) => ({type: CHANGE_INPUTS, name: e.target.name, value: e.target.value});

export const postEncodeRequest = () => ({type: POST_ENCODE_REQUEST});
export const postEncodeSuccess = value => ({type: POST_ENCODE_SUCCESS, payload: value});
export const postEncodeFailure = () => ({type: POST_ENCODE_FAILURE});

export const postDecodeRequest = () => ({type: POST_DECODE_REQUEST});
export const postDecodeSuccess = value => ({type: POST_DECODE_SUCCESS, payload: value});
export const postDecodeFailure = () => ({type: POST_DECODE_FAILURE});

export const doEncode = (input) => {
    return async (dispatch) => {
        if (input.password !== '' && input.encoded === '') {
            try {
                dispatch(postEncodeRequest());
                const post = await axios.post('http://localhost:8000/code/encode', {
                    message: input.decoded,
                    password: input.password
                });
                dispatch(postEncodeSuccess(post.data.encoded));
            } catch (error) {
                dispatch(postEncodeFailure(error));
            }
        }
    }
};

export const doDecode = (input) => {
    return async (dispatch) => {
        if (input.password !== '' && input.decoded === '') {
            try {
                dispatch(postDecodeRequest());
                const post = await axios.post('http://localhost:8000/code/decode', {
                    message: input.encoded,
                    password: input.password
                });
                dispatch(postDecodeSuccess(post.data.decoded));
            } catch (error) {
                dispatch(postDecodeFailure(error));
            }
        }
    }
};