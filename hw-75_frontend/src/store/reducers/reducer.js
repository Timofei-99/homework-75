import {
    CHANGE_INPUTS, POST_DECODE_FAILURE,
    POST_DECODE_REQUEST, POST_DECODE_SUCCESS,
    POST_ENCODE_FAILURE,
    POST_ENCODE_REQUEST,
    POST_ENCODE_SUCCESS
} from "../actions/action";

const initialState = {
    decoded: '',
    password: '',
    encoded: ''
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_ENCODE_REQUEST:
            return {...state,}
        case POST_ENCODE_SUCCESS:
            return {...state, encoded: action.payload, decoded: ''}
        case POST_ENCODE_FAILURE:
            return {...state,}
        case POST_DECODE_REQUEST:
            return {...state}
        case POST_DECODE_SUCCESS:
            return {...state, encoded: '', decoded: action.payload}
        case POST_DECODE_FAILURE:
            return {...state}
        case CHANGE_INPUTS:
            return {...state, [action.name]: action.value}
        default:
            return state;
    }
};