import React from 'react';
import './Forms.css';
import {useDispatch, useSelector} from "react-redux";
import {changeInputs, doDecode, doEncode} from "../../store/actions/action";

const Forms = () => {
    const dispatch = useDispatch();
    const inputs = useSelector(state => state);


    return (
        <div className="Forms">
            <div className="area">
                <label>Decoded message</label>
                <textarea value={inputs.decoded} onChange={(e) => dispatch(changeInputs(e))} name="decoded"
                          cols={30} rows={10}/>
            </div>
            <div className="password">
                <label>Password</label>
                <input value={inputs.password} onChange={(e) => dispatch(changeInputs(e))} name="password"
                       type='text'/>
                <button onClick={() => dispatch(doEncode(inputs))}>Encode</button>
                <button onClick={() => dispatch(doDecode(inputs))}>Decode</button>
            </div>
            <div className="area">
                <label>Encoded message</label>
                <textarea value={inputs.encoded} onChange={(e) => dispatch(changeInputs(e))} name="encoded"
                          cols={30} rows={10}/>
            </div>
        </div>
    );
};

export default Forms;